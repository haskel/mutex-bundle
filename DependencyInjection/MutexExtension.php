<?php
namespace Haskel\MutexBundle\DependencyInjection;

use Haskel\MutexBundle\Adapter\FileAdapter;
use Haskel\MutexBundle\Adapter\RedisAdapter;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;

class MutexExtension extends Extension
{
    /**
     * @var ContainerBuilder
     */
    private $container;

    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new Loader\XmlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.xml');

        $this->container = $container;

        $mutexService = $container->getDefinition('haskel.mutex');
        $adapter      = $this->getAdapter($config);
        if ($adapter) {
            $container->setDefinition('haskel.mutex.adapter.' . $config['adapter'], $adapter);
            $mutexService->addMethodCall('setAdapter', [$adapter]);
        }
    }

    /**
     * @param $config
     *
     * @return null|Definition
     */
    private function getAdapter($config)
    {
        $adapter = null;
        switch ($config['adapter']) {
            case 'file';
                $adapter = $this->getFileAdapter($config);
                break;

            case 'redis';
                $adapter = $this->getRedisAdapter($config);
                break;
        }

        return $adapter;
    }

    /**
     * @param $config
     *
     * @return Definition
     */
    private function getRedisAdapter($config)
    {
        $adapter = new Definition();
        $adapter->setClass(RedisAdapter::class);
        $redis = $this->container->getDefinition('redis');
        $arguments = [$redis];
        if (isset($config['adapter_parameters']['key_prefix'])) {
            $arguments[] = $config['adapter_parameters']['key_prefix'];
        }
        $adapter->setArguments($arguments);

        return $adapter;
    }

    /**
     * @param $config
     *
     * @return Definition
     */
    private function getFileAdapter($config)
    {
        $adapter = new Definition();
        $adapter->setClass(FileAdapter::class);
        $arguments = [$config['adapter_parameters']['files_dir']];
        if (isset($config['adapter_parameters']['hash_algo'])) {
            $arguments[] = $config['adapter_parameters']['hash_algo'];
        }
        $adapter->setArguments($arguments);

        return $adapter;
    }
}