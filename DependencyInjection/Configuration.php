<?php
namespace Haskel\MutexBundle\DependencyInjection;

use Symfony\Component\Config\Definition\ConfigurationInterface;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;

class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('mutex');
        $rootNode->children()
            ->scalarNode('adapter')->defaultValue('file')->end()
            ->arrayNode('adapter_parameters')
                ->children()
                    ->scalarNode('files_dir')->defaultValue('/tmp')->end()
                    ->scalarNode('key_prefix')->defaultValue('')->end()
                ->end()
            ->end()
        ->end();

        return $treeBuilder;
    }
}