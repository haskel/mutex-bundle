<?php
namespace Haskel\MutexBundle;

use Haskel\MutexBundle\Adapter\Adapter;
use Haskel\MutexBundle\Exception\AcquireException;
use Haskel\MutexBundle\Mutex\ExpiringMutex;
use Haskel\MutexBundle\Mutex\Mutex;
use Closure;
use Exception;
use Haskel\MutexBundle\Mutex\SelfReleasableMutex;
use Psr\Log\LoggerInterface;

class MutexManager
{
    /**
     * How long time wait for the next attempt to acquire the lock
     *
     * @var int
     */
    private $acquireAttemptPeriod = 1;

    /**
     * @var Adapter
     */
    private $storageAdapter;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var array
     */
    private $acquiredMutexList = [];

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
        register_shutdown_function([$this, 'releaseAllIfFatal']);
    }

    /**
     * @param Adapter $storageAdapter
     * @todo: move to constructor arguments
     */
    public function setAdapter(Adapter $storageAdapter)
    {
        $this->storageAdapter = $storageAdapter;
    }

    /**
     * @param Mutex $mutex
     *
     * @return string
     */
    private function getLockKey(Mutex $mutex)
    {
        $classHash = md5(strtolower(get_class($mutex)));

        return "{$classHash}_{$mutex->getKey()}";
    }

    /**
     * @param Mutex $mutex
     * @param int   $waitTimeout
     */
    public function acquire(Mutex $mutex, $waitTimeout = 0)
    {
        $action = function () use ($mutex) {
            $lockKey = $this->getLockKey($mutex);
            // @todo: make it atomic
            if ($this->storageAdapter->exists($lockKey)) {
                throw new AcquireException("key '{$lockKey}' already acquired");
            }
            if ($mutex instanceof SelfReleasableMutex) {
                $mutex->setManager($this);
            }
            $this->storageAdapter->create($lockKey, $mutex->getContext());
            if ($mutex instanceof SelfReleasableMutex) {
                $mutex->setIsAcquired(true);
            }
            $mutexId = spl_object_hash($mutex);
            $this->acquiredMutexList[$mutexId] = $mutex;
        };
        $action->bindTo($this);

        $attemptsCount = floor($waitTimeout / $this->acquireAttemptPeriod);
        $attemptsCount = $attemptsCount ?: 1;
        $this->attempt($action, $attemptsCount, $this->acquireAttemptPeriod);
    }

    /**
     * @param Mutex $mutex
     */
    public function release(Mutex $mutex)
    {
        $lockKey = $this->getLockKey($mutex);
        $mutexId = spl_object_hash($mutex);
        if ($this->storageAdapter->exists($lockKey) && isset($this->acquiredMutexList[$mutexId])) {
            $this->storageAdapter->delete($lockKey);
        }
    }

    /**
     * Release all mutexes if fatal error
     * todo: add property which indicate that you don't need to release if fatal
     */
    public function releaseAllIfFatal()
    {
        $error = error_get_last();
        if (!$error) {
            return;
        }
        if (isset($error['type']) && $error['type'] !== E_ERROR) {
            return;
        }
        foreach ($this->acquiredMutexList as $mutex) {
            $this->release($mutex);
        }
    }

    /**
     * @param Mutex $mutex
     *
     * @return bool
     */
    public function isAcquired(Mutex $mutex)
    {
        $lockKey = $this->getLockKey($mutex);

        return $this->storageAdapter->exists($lockKey);
    }

    /**
     * @param Closure $action
     * @param int     $attemptsCount
     *
     * @return bool
     */
    private function attempt(Closure $action, $attemptsCount = 1, $waitSeconds = 1)
    {
        if ($waitSeconds < 1) {
            $waitSeconds = 1;
        }

        foreach (range(1, $attemptsCount) as $attemptNumber) {
            try {
                $action();
                return true;
            } catch (Exception $e) {
                $this->logger->error(sprintf("attempt #%d: %s", $attemptNumber, $e->getMessage()));
                if ($attemptNumber == $attemptsCount) {
                    throw $e;
                }
            }
            sleep((int) $waitSeconds);
        }
    }

    public function isExpired(ExpiringMutex $mutex)
    {

    }

    public function whoAcquired(Mutex $mutex)
    {

    }
}