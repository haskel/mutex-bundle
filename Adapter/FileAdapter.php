<?php
namespace Haskel\MutexBundle\Adapter;

use Haskel\MutexBundle\Exception\AdapterException;
use Symfony\Component\Filesystem\Filesystem;

class FileAdapter implements Adapter
{
    /**
     * @var string
     */
    private $filesDir;

    /**
     * @var string
     */
    private $hashAlgo;

    /**
     * @param string      $filesDir
     * @param string|null $hashAlgo
     */
    public function __construct($filesDir, $hashAlgo = null)
    {
        $this->filesDir = $filesDir;
        $this->hashAlgo = $hashAlgo ?: 'sha256';
        $this->checkFilesDir($filesDir);
    }

    /**
     * @param $lockKey
     *
     * @return string
     */
    private function getFileName($lockKey)
    {
        return $this->filesDir . hash($this->hashAlgo, $lockKey) . ".lock";
    }

    /** {@inheritdoc} */
    public function create($lockKey, $context)
    {
        $file = $this->getFileName($lockKey);
        // todo: overwrite file if exists, but not locked LOCK_EX. & log context from previous lock-file
        if (file_exists($file)) {
            throw new AdapterException("Lock file already exists. Lock key: {$lockKey}");
        }
//        if (!stream_supports_lock(STDOUT)) {
//            throw new AdapterException('Lock not supported by your filesystem');
//        }
        $currentMask = umask(0333); // 0444 perms for file
        $created = file_put_contents($file, json_encode($context), LOCK_EX|LOCK_NB); // LOCK_NB don't wait for blocking
        umask($currentMask);
        if ($created === false) {
            throw new AdapterException("Lock file not created. Lock key: {$lockKey}");
        }
    }

    /** {@inheritdoc} */
    public function delete($lockKey)
    {
        $file = $this->getFileName($lockKey);
        unlink($file);
    }

    /** {@inheritdoc} */
    public function exists($lockKey)
    {
        $file = $this->getFileName($lockKey);

        return file_exists($file);
    }

    /** {@inheritdoc} */
    public function get($lockKey)
    {
        $file = $this->getFileName($lockKey);
        if (!file_exists($file)) {
            throw new AdapterException("Lock file does not exist. Lock key: {$lockKey}");
        }
        $data = file_get_contents($file);

        return json_encode($data, true);
    }

    /**
     * @param $filesDir
     *
     * @throws AdapterException
     */
    public function checkFilesDir($filesDir)
    {
        $fs = new Filesystem();
        
        if (!$fs->exists($filesDir)) {
            $fs->mkdir($filesDir);
        }

        if (!is_writable($filesDir)) {
            throw new AdapterException("Directory {$filesDir} is not available to write");
        }
    }
}