<?php
namespace Haskel\MutexBundle\Adapter;

use Haskel\MutexBundle\Mutex\Mutex;

interface Adapter
{
    /**
     * @param $lockKey
     * @param $context
     *
     * @return Mutex
     */
    public function create($lockKey, $context);

    /**
     * @param $lockKey
     *
     * @return void
     */
    public function delete($lockKey);

    /**
     * @param $lockKey
     *
     * @return bool
     */
    public function exists($lockKey);

    /**
     * @param $lockKey
     *
     * @return Mutex
     */
    public function get($lockKey);
}