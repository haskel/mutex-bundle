<?php
namespace Haskel\MutexBundle\Mutex;

abstract class AbstractExpiringMutex
extends AbstractMutex
implements ExpiringMutex
{
    private $expiration;
    private $ttl;
}