<?php
namespace Haskel\MutexBundle\Mutex;

interface Mutex
{
    /**
     * @return string
     */
    public function getKey();

    /**
     * @return array
     */
    public function getContext();
}