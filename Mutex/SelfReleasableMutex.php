<?php
namespace Haskel\MutexBundle\Mutex;

use Haskel\MutexBundle\MutexManager;

interface SelfReleasableMutex
{
    /**
     * @param MutexManager $manager
     *
     * @return mixed
     */
    public function setManager(MutexManager $manager);

    public function __destruct();

    /**
     * @param bool $isAcquired
     *
     * @return void
     */
    public function setIsAcquired($isAcquired);

    /**
     * @return bool
     */
    public function isAcquired();
}