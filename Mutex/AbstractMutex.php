<?php
namespace Haskel\MutexBundle\Mutex;

use Haskel\MutexBundle\MutexManager;

abstract class AbstractMutex implements Mutex, SelfReleasableMutex
{
    /**
     * @var bool
     */
    protected $isAcquired = false;

    /**
     * @var MutexManager
     */
    protected $mutexManager;

    /** {@inheritdoc} */
    abstract public function getKey();

    /** {@inheritdoc} */
    public function getContext()
    {
        $trace = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS);

        return $trace;
    }

    /** {@inheritdoc} */
    public function setManager(MutexManager $manager)
    {
        $this->mutexManager = $manager;
    }

    /** {@inheritdoc} */
    public function __destruct()
    {
        if (!$this->isAcquired()) {
            return;
        }
        $this->mutexManager->release($this);
    }

    /** {@inheritdoc} */
    public function setIsAcquired($isAcquired)
    {
        $this->isAcquired = $isAcquired;
    }

    /** {@inheritdoc} */
    public function isAcquired()
    {
        return $this->isAcquired;
    }
}