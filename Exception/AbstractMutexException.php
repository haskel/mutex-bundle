<?php
namespace Haskel\MutexBundle\Exception;

use Exception;

abstract class AbstractMutexException
extends Exception
implements MutexException
{

}